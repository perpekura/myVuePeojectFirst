// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import App from './App'
import router from './router'
import VueResource from 'vue-resource'

import './lib/Mui/css/mui.min.css'
import './lib/Mui/css/icons-extra.css'
import './lib/Mui/css/app.css'

import { Swipe, SwipeItem } from 'mint-ui';
Vue.component("mt-swipe", Swipe);
Vue.component("mt-swipe-item", SwipeItem);

Vue.use(VueResource)

import { Header } from 'mint-ui';

Vue.component("myheader", Header);

Vue.config.productionTip = false

/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  components: { App },
  template: '<App/>'
})
