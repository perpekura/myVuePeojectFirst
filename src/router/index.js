import Vue from 'vue'
import Router from 'vue-router'
import home from '../components/tabbar/Home.vue'
import member from '../components/tabbar/Member.vue'
import shopcar from '../components/tabbar/Shopcar.vue'
import search from '../components/tabbar/Search.vue'
import newslist from '../components/news/NewsList.vue'


Vue.use(Router)

export default new Router({
  routes: [
    {path:'/',redirect:'/home'},
    {path:'/home',component:home},
      {path:'/member',component:member},
      {path:'/shopcar',component:shopcar},
      {path:'/search',component:search},
      {path:'/home/newlist',component:newslist}
      
    
  ],
  linkActiveClass:'mui-active'
})
